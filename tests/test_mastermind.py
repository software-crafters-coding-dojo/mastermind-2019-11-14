from enum import Enum

from mastermind import __version__


def test_version():
    assert __version__ == '0.1.0'


class colors(Enum):
    BLUE = "blue"
    RED = "red"
    PINK = "pink"
    PURPLE = "purple"
    WITHE = "withe"
    GREEN = "green"


class Minstermind(object):

    def __init__(self, solution):
        self.solution = solution

    def evaluate(self, combinaison):
        well_placed = 0
        missplaced = 0
        miss_solution = [item for item in self.solution]
        for position in range(0, 4):
            if self.solution[position] is combinaison[position]:
                well_placed += 1
                self.supress_color_solution(combinaison, miss_solution, position)
            elif combinaison[position] in miss_solution:
                missplaced += 1
                self.supress_color_solution(combinaison, miss_solution, position)
        return (well_placed, missplaced)

    def supress_color_solution(self, combinaison, miss_solution, position):
        try:
            miss_solution.pop(miss_solution.index(combinaison[position]))
        except ValueError:
            pass


def test_all_well_positioned_color():
    combinaison: list = [colors.BLUE, colors.RED, colors.RED, colors.RED]

    minstermind = Minstermind(combinaison)
    result = minstermind.evaluate(combinaison)

    assert result == (4, 0)


def test_one_well_positioned_color():
    right_combinaison: list = [colors.BLUE, colors.RED, colors.RED, colors.RED]
    wrong_combinaison: list = [colors.BLUE, colors.BLUE, colors.BLUE, colors.BLUE]

    minstermind = Minstermind(right_combinaison)
    result = minstermind.evaluate(wrong_combinaison)

    assert result == (1, 0)


def test_one_not_well_positioned_rest_wrong():
    right_combinaison: list = [colors.BLUE, colors.GREEN, colors.RED, colors.WITHE]
    wrong_combinaison: list = [colors.GREEN, colors.PURPLE, colors.PURPLE, colors.PURPLE]

    minstermind = Minstermind(right_combinaison)
    result = minstermind.evaluate(wrong_combinaison)

    assert result == (0, 1)


def test_two_not_well_positioned_rest_missplaced():
    right_combinaison: list = [colors.BLUE, colors.GREEN, colors.RED, colors.WITHE]
    wrong_combinaison: list = [colors.GREEN, colors.BLUE, colors.PURPLE, colors.PINK]

    minstermind = Minstermind(right_combinaison)
    result = minstermind.evaluate(wrong_combinaison)

    assert result == (0, 2)

def test_two_not_well_positioned_rest_two_missplaced():
    right_combinaison: list = [colors.BLUE, colors.GREEN, colors.RED, colors.WITHE]
    wrong_combinaison: list = [colors.GREEN, colors.BLUE, colors.RED, colors.WITHE]

    minstermind = Minstermind(right_combinaison)
    result = minstermind.evaluate(wrong_combinaison)

    assert result == (2, 2)

def test_mastermind():
    right_combinaison: list = [colors.BLUE, colors.GREEN, colors.RED, colors.WITHE]
    wrong_first_combinaison: list = [colors.GREEN, colors.BLUE, colors.RED, colors.WITHE]
    wrong_two_combinaison: list = [colors.GREEN, colors.GREEN, colors.RED, colors.WITHE]

    minstermind = Minstermind(right_combinaison)
    result = minstermind.evaluate(wrong_first_combinaison)

    assert result == (2, 2)

    result = minstermind.evaluate(wrong_two_combinaison)

    assert result == (2, 1)
